import axios from 'axios'
import React, { useState } from 'react'
import BASE_URL from '../constants/url'
import { useNavigate } from 'react-router-dom'

const AddTicket = () => {
  const [loading, setLoading] = useState(true)
  const navigate = useNavigate()

  const [formData, setFormData] = useState({
    title: '',
    description: '',
    severity: '',
    assign: '',
  })

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      const response = await axios.post(
        `${BASE_URL.URL_GATEWAY}/issue/ADM602298709`,
        {
          title: formData.title,
          description: formData.description,
          severity: formData.severity,
          assign: formData.assign,
        }
      )
      setLoading(false)
      alert(response.data.message)
      navigate('/')
      return
    } catch (error) {
      setLoading(false)
      alert('Unable to save your ticket', error.message)
      navigate('/')
      return
    }
  }

  return (
    <>
      <div className='row  my-2'>
        <form onSubmit={handleSubmit}>
          <h4>Add Ticket Details</h4>
          <div className='row'>
            <div className='col'>
              <label htmlFor='middle_name' className='form-label'>
                Ticket Title <span className='text-danger'>*</span>
              </label>

              <input
                type='text'
                className='form-control'
                placeholder='Enter title of your issue'
                name='middle_name'
                value={formData.title}
                onChange={e => {
                  setFormData({ ...formData, title: e.target.value })
                }}
              />
            </div>
            <div className='col'>
              <label htmlFor='middle_name' className='form-label'>
                Severity <span className='text-danger'>*</span>
              </label>

              <select
                value={formData.severity}
                name='severity'
                onChange={e => {
                  setFormData({ ...formData, severity: e.target.value })
                }}
                className='form-select'
              >
                <option value='' disabled>
                  Choose ..
                </option>
                <option value='Low'>Low</option>
                <option value='Medium'>Medium</option>
                <option value='High'>High</option>
              </select>
            </div>
          </div>
          <div className='row my-3'>
            <div className='col-6 '>
              <label htmlFor='middle_name' className='form-label'>
                Assign To <span className='text-danger'>*</span>
              </label>

              <input
                type='text'
                className='form-control'
                placeholder='Assign person'
                name='assign'
                value={formData.assign}
                onChange={e => {
                  setFormData({ ...formData, assign: e.target.value })
                }}
              />
            </div>
          </div>
          <div className='row my-3'>
            <div className='col-12 '>
              <label htmlFor='middle_name' className='form-label'>
                Issue description <span className='text-danger'>*</span>
              </label>

              <textarea
                type='text'
                className='form-control'
                placeholder='Povide detailed descritpition of your issue'
                rows={5}
                name='description'
                value={formData.description}
                onChange={e => {
                  setFormData({ ...formData, description: e.target.value })
                }}
              />
            </div>
          </div>
          <input type='submit' className='btn btn-primary' value='Add Ticket' />
        </form>
      </div>
    </>
  )
}

export default AddTicket
