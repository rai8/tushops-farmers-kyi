import axios from 'axios'
import React, { useState } from 'react'
import FarmerDetails from '../components/FarmerDetails'
import PersonalDetails from '../components/PersonalDetails'
import URL_TUSHOPS from '../constants/url'
import { useNavigate } from 'react-router-dom'
import validator from 'validator'

const FormPage = () => {
  const [step, setStep] = useState(0)
  const [formData, setFormData] = useState({
    first_name: '',
    middle_name: '',
    last_name: '',
    email: '',
    phone_number: '',
    type_of_farmer: '',
    farm_size: '',
  })

  const navigate = useNavigate()
  const tileHeadings = ['Personal Details', 'Farm Details']
  const [error, setError] = useState(false)
  const [serverError, setServerError] = useState(null)
  const [disableBtn, setDisableBtn] = useState(false)

  //handle prev
  const handlePrev = () => {
    setStep(curr => curr - 1)
    setDisableBtn(false)
  }
  //hanlde next
  const handleNext = () => {
    if (step === tileHeadings.length - 1) {
      //save to database in express

      console.log('handle-form submission')

      //   console.log(formData)
      axios
        .post(`${URL_TUSHOPS}/farmers`, { formData })
        .then(res => {
          console.log(res)
          alert('Details successfully recieved')
          navigate('/')
          return
        })
        .catch(err => {
          setError(true)
          setServerError(err.response.data)
          console.log(err.response.data)
        })
    } else {
      switch (step) {
        case 0:
          if (
            validator.isEmpty(formData.first_name) ||
            validator.isEmpty(formData.last_name) ||
            validator.isEmpty(formData.middle_name) ||
            validator.isEmpty(formData.email) ||
            validator.isEmpty(formData.phone_number)
          ) {
            setError(true)
          } else {
            setStep(curr => curr + 1)
          }
          break
        default:
          return null
      }
    }
  }

  const displayForm = () => {
    switch (step) {
      case 0:
        return <PersonalDetails formData={formData} setFormData={setFormData} />
      case 1:
        return (
          <FarmerDetails
            formData={formData}
            setFormData={setFormData}
            disableBtn={disableBtn}
            setDisableBtn={setDisableBtn}
          />
        )

      default:
        return <PersonalDetails formData={formData} setFormData={setFormData} />
      // break;
    }
  }
  return (
    <>
      <div className='form-container'>
        {' '}
        <div className='form-heading'>
          <p className='py-2 px-2 text-center heading'>{tileHeadings[step]}</p>
        </div>
        {error === true ? (
          <p className='error-message text-danger'>
            Kindly fill in all required fields
          </p>
        ) : null}
        {error === true ? (
          <p className='error-message text-danger'>{serverError}</p>
        ) : null}
        <div className='form-body my-4'>{displayForm()}</div>
        <div className='d-flex justify-content-between form-btn'>
          <button
            disabled={step <= 0}
            onClick={handlePrev}
            className='btn btn-primary px-4 btn-2'
          >
            Prev
          </button>
          <button
            // disabled={true}
            disabled={disableBtn === true}
            onClick={handleNext}
            className='btn btn-primary px-4 btn-2 '
          >
            {step === tileHeadings.length - 1 ? 'Submit' : 'Next'}
          </button>
        </div>
      </div>
    </>
  )
}

export default FormPage
