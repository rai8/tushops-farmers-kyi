import axios from 'axios'
import React, { useEffect, useState } from 'react'
import BASE_URL from '../constants/url'
import moment from 'moment'
import { Link, useNavigate } from 'react-router-dom'

const MainPage = () => {
  const [issues, setIssues] = useState([])
  const [loading, setLoading] = useState(true)
  let navigate = useNavigate()

  const colorUse = status => {
    return status === 'open'
      ? 'col spacing badge bg-primary px-0 py-1 statusbtn '
      : status === 'backlog'
      ? 'col spacing badge bg-danger px-0 py-1 '
      : status === 'pending'
      ? 'col spacing badge bg-warning px-0 py-1 '
      : status === 'complete'
      ? 'col spacing badge bg-success px-0 py-1 '
      : 'col spacing'
  }

  useEffect(() => {
    const fetchIssuesLog = async () => {
      try {
        const response = await axios.get(`${BASE_URL.URL_GATEWAY}/issue`)
        const data = response.data.data
        setIssues(data)
        setLoading(false)
      } catch (err) {
        console.log(err.message)
        setLoading(false)
      }
    }
    fetchIssuesLog()
  }, [])

  const handleClick = detail => {
    navigate(`/ticket/${detail.id}`, {
      state: { issue: detail },
    })
  }
  return (
    <>
      {loading === false ? (
        <div className='main'>
          <div className='d-flex justify-content-between pb-2'>
            <h4 className='text-dark'> All Tickets</h4>
            <Link to='/add-ticket' className='btn btn-primary'>
              Add Ticket
            </Link>
          </div>
          <div className='row'>
            <div className='row'>
              <p className='col-2  fw-bolder fs-6'>Ticket ID</p>
              <p className='col-3 fw-bolder fs-6'>Issue Summary </p>
              <p className='col fw-bolder fs-6'>Assigned</p>
              <p className='col fw-bolder fs-6'>Severity </p>
              <p className='col fw-bolder fs-6'>Status</p>
              <p className='col fw-bolder fs-6'>Posted</p>
              <p className='col fw-bolder fs-6'>View</p>
            </div>
            <hr />
            {issues &&
              issues.map(issue => {
                return (
                  <div key={issue.id} className='row'>
                    <p className='col-2  spacing'>{issue.id}</p>
                    <p className='col-3  spacing'>{issue.title}</p>
                    <p className='col spacing'>{issue.assign}</p>
                    <p className='col spacing'>{issue.severity}</p>
                    <p className={colorUse(issue.status_set.status)}>
                      {issue.status_set.status}
                    </p>
                    <p className='col spacing pl-4'>
                      {moment(issue.created_at).format('L')}
                    </p>
                    <p className='col spacing'>
                      <span className='row row-sm-12 row-md-12 row-sm-12'>
                        <i
                          className='bi bi-eye text-primary col cols-xs-5 col-sm-5 col-md-5 infobtn'
                          onClick={() => handleClick(issue)}
                        ></i>

                        <a href='/' className='col cols-xs-5 col-sm-5 col-md-5'>
                          <i className='bi bi-trash text-danger'></i>
                        </a>
                      </span>
                    </p>
                  </div>
                )
              })}
          </div>
        </div>
      ) : (
        <div className='loading row d-flex justify-content-center  align-items-center'>
          <div className='spinner-border text-warning' role='status'>
            <span className='visually-hidden'>Loading...</span>
          </div>
        </div>
      )}
    </>
  )
}

export default MainPage
