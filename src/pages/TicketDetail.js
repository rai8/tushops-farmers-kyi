import React, { useState } from 'react'
import CommentForm from '../components/CommentForm'
import CommentSection from '../components/CommentSection'
import { useLocation } from 'react-router-dom'
import moment from 'moment'

const TicketDetail = () => {
  let location = useLocation()
  const detail = location.state.issue
  // console.log('details', detail)
  const [displayComment, setDisplayCommentForm] = useState(true)
  const [formDetails, setFormDetails] = useState({
    adminId: '',
    issueId: '',
    comment: '',
  })

  const colorUse = status => {
    return status === 'open'
      ? 'fw-bolder badge bg-primary mx-2 py-1 mb-0 '
      : status === 'backlog'
      ? 'fw-bolder badge bg-danger mx-2 py-1 mb-0 '
      : status === 'pending'
      ? 'fw-bolder badge bg-warning mx-2 py-1 mb-0 '
      : status === 'complete'
      ? 'fw-bolder badge bg-success mx-2 py-1 mb-0 '
      : 'fw-bolder'
  }

  return (
    <>
      <div className='row'>
        <div className='d-grid gap-2 d-md-flex justify-content-md-end my-2 '>
          <button className='btn btn-danger me-md-2' type='button'>
            Delete Ticket
          </button>
        </div>
        <div className='border cont'>
          <div className='col'>
            <div className='row'>
              <div className='col-12'>
                <div className='row'>
                  <p className='fw-bolder fs-4'>
                    <span>Ticket No: </span>{' '}
                    <span className=''>{detail.id}</span>
                  </p>
                </div>
                <div className='row'>
                  <div className='col'>
                    <p className='pfont'>
                      <span>Posted By: </span>{' '}
                      <span className='fw-bolder'>{detail.admin.name}</span>
                    </p>
                    <p className='pfont'>
                      <span>Severity: </span>{' '}
                      <span className='fw-bolder'>{detail.severity}</span>
                    </p>
                  </div>
                  <div className='col'>
                    <p className='pfont'>
                      <span>Status: </span>{' '}
                      <span className={colorUse(detail.status_set.status)}>
                        {detail.status_set.status}
                      </span>
                    </p>
                    <p className='pfont'>
                      <span>Date Posted: </span>{' '}
                      <span className='fw-bolder'>
                        {' '}
                        {moment(detail.created_at).format('L')}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <hr className='my-2' />
              <div className='col-12'>
                <p>{detail.description}</p>
              </div>
            </div>
            <div className='row'>
              <p className='fw-bolder fs-4'>Comments</p>
              {displayComment === true ? (
                <CommentSection comments={detail.comments} />
              ) : (
                <CommentForm />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TicketDetail
