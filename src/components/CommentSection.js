import React from 'react'
import moment from 'moment'

const CommentSection = ({ comments }) => {
  // console.log('comment section', comments)
  return (
    <>
      {comments.length !== 0 ? (
        <div className='actionBox'>
          <ul className='commentList'>
            {comments &&
              comments.map(comment => {
                console.log(comment.admins.name)
                return (
                  <li key={comment.id}>
                    <div className='commenterImage'>
                      <i className='bi bi-person-fill'></i>
                    </div>
                    <div className='commentText'>
                      <p>{comment.comment}</p>
                      <span className='date sub-text'>
                        on{' '}
                        {`${moment(comment.created_at).format('L')} - ${
                          comment.admins.name
                        } `}
                      </span>
                    </div>
                  </li>
                )
              })}
          </ul>
        </div>
      ) : (
        <div>
          <p>No comments</p>
        </div>
      )}
      <div>
        <a href='/'>
          {' '}
          <i className='bi bi-chat-left-dots fs-2'></i>{' '}
        </a>
      </div>
    </>
  )
}

export default CommentSection
