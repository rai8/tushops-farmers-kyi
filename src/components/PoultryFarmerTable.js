import React from 'react'

const PoultryFarmerTable = ({ poultry_farmers }) => {
  const bronze_farmers = poultry_farmers.filter(nf => nf.tier === 'Bronze')

  //get platinum users
  const platinum_farmers = poultry_farmers.filter(
    plf => plf.tier === 'Platinum'
  )
  console.log(platinum_farmers)

  //get gold users
  const gold_farmers = poultry_farmers.filter(gf => gf.tier === 'Gold')

  //get silver user
  const silver_farmers = poultry_farmers.filter(sf => sf.tier === 'Silver')
  return (
    <>
      <p className='h3 my-2'>Poultry Farmer's Table</p>
      <div>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th scope='col'>Full Name</th>
              <th scope='col'>Email</th>
              <th scope='col'>Phone Number</th>
              <th scope='col'>Poultry Farm Size</th>
              <th scope='col'>Chicken Count</th>
              <th scope='col'>Quantity Produced Weekly</th>
              <th scope='col'>Tier</th>
            </tr>
          </thead>
          {poultry_farmers.length > 0 ? (
            <tbody>
              {/* platinum farmers */}
              {platinum_farmers &&
                platinum_farmers.map(plf => {
                  return (
                    <tr key={plf.id}>
                      <td>{`${plf.last_name} ${plf.first_name} ${plf.middle_name}`}</td>
                      <td>{plf.email}</td>
                      <td>{plf.phone_number}</td>
                      <td>{plf.farm_size}</td>
                      <td>{plf.chicken_count}</td>
                      <td>{plf.qty_produced_weekly}</td>
                      <td>{plf.tier}</td>
                    </tr>
                  )
                })}
              {/* gold farmers */}
              {gold_farmers &&
                gold_farmers.map(gf => {
                  return (
                    <tr key={gf.id}>
                      <td>{`${gf.last_name} ${gf.first_name} ${gf.middle_name}`}</td>
                      <td>{gf.email}</td>
                      <td>{gf.phone_number}</td>
                      <td>{gf.farm_size}</td>
                      <td>{gf.chicken_count}</td>
                      <td>{gf.qty_produced_weekly}</td>
                      <td>{gf.tier}</td>
                    </tr>
                  )
                })}
              {/* silver farmers */}
              {silver_farmers.length === 0
                ? null
                : silver_farmers.map(sf => {
                    return (
                      <tr key={sf.id}>
                        <td>{`${sf.last_name} ${sf.first_name} ${sf.middle_name}`}</td>
                        <td>{sf.email}</td>
                        <td>{sf.phone_number}</td>
                        <td>{sf.farm_size}</td>
                        <td>{sf.chicken_count}</td>
                        <td>{sf.qty_produced_weekly}</td>
                        <td>{sf.tier}</td>
                      </tr>
                    )
                  })}
              {/* bronze farmers */}
              {bronze_farmers &&
                bronze_farmers.map(bf => {
                  return (
                    <tr key={bf.id}>
                      <td>{`${bf.last_name} ${bf.first_name} ${bf.middle_name}`}</td>
                      <td>{bf.email}</td>
                      <td>{bf.phone_number}</td>
                      <td>{bf.farm_size}</td>
                      <td>{bf.chicken_count}</td>
                      <td>{bf.qty_produced_weekly}</td>
                      <td>{bf.tier}</td>
                    </tr>
                  )
                })}
            </tbody>
          ) : (
            <tbody>
              <tr>
                <td className='text-danger' colSpan={7}>
                  No Farmers currently existing in database .
                </td>
              </tr>
            </tbody>
          )}
        </table>
      </div>
    </>
  )
}

export default PoultryFarmerTable
