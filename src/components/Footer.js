import React from 'react'

const Footer = () => {
  return (
    <>
      <footer>
        <div className='container'>
          <p className='footer-p'>
            Copyright {new Date().getFullYear()} @ Tushops
          </p>
        </div>
      </footer>
    </>
  )
}

export default Footer
