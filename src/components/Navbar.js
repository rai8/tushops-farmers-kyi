import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <>
      <nav className='navbar   bg-gradient'>
        <div className='container'>
          <Link className='navbar-brand nav-title' to='/'>
            Tushop Issue Register
          </Link>

          <button
            className='navbar-toggler'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#navbarNav'
            aria-controls='navbarNav'
            aria-expanded='false'
            aria-label='Toggle navigation'
          >
            <span className='navbar-toggler-icon'></span>
          </button>
          <div className='collapse navbar-collapse' id='navbarNav'>
            <ul className='navbar'>
              <li className='nav-item text-dark'>
                <a className='nav-link mylinks' aria-current='page' href='/'>
                  Home
                </a>
              </li>

              <li className='nav-item'>
                <a className='nav-link mylinks' aria-current='page' href='/'>
                  My Tickets
                </a>
              </li>

              <li className='nav-item'>
                <a className='nav-link mylinks' href='/'>
                  Issues
                </a>
              </li>

              <li className='nav-item'>
                <a className='nav-link mylinks' href='/'>
                  Tickets
                </a>
              </li>
            </ul>
          </div>
          <div>
            <div className='dropdown'>
              <button
                className='btn btn-secondary  text-light border-0'
                type='button'
                aria-expanded='false'
              >
                Logout
              </button>
            </div>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar
