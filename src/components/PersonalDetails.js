import React from 'react'

const PersonalDetails = ({ formData, setFormData }) => {
  return (
    <>
      <div className='col-12 my-2'>
        <label htmlFor='first_name' className='form-label'>
          First Name <span className='text-danger'>*</span>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Enter first name'
          name='first_name'
          value={formData.first_name}
          onChange={e => {
            setFormData({ ...formData, first_name: e.target.value })
          }}
        />
      </div>
      <div className='col-12 my-2'>
        <label htmlFor='last_name' className='form-label'>
          Last Name <span className='text-danger'>*</span>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Enter last name'
          name='last_name'
          value={formData.last_name}
          onChange={e => {
            setFormData({ ...formData, last_name: e.target.value })
          }}
        />
      </div>
      <div className='col-12 my-2'>
        <label htmlFor='middle_name' className='form-label'>
          Middle name <span className='text-danger'>*</span>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Enter middle name'
          name='middle_name'
          value={formData.middle_name}
          onChange={e => {
            setFormData({ ...formData, middle_name: e.target.value })
          }}
        />
      </div>
      <div className='col-12 my-2'>
        <label htmlFor='email' className='form-label'>
          Email <span className='text-danger'>*</span>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Enter email'
          value={formData.email}
          name='email'
          onChange={e => {
            setFormData({ ...formData, email: e.target.value })
          }}
        />
      </div>
      <div className='col-12 my-2'>
        <label htmlFor='phone_number' className='form-label'>
          Phone Number <span className='text-danger'>*</span>
        </label>
        <input
          type='text'
          className='form-control'
          placeholder='Enter phone number'
          name='phone_number'
          value={formData.phone_number}
          onChange={e => {
            setFormData({ ...formData, phone_number: e.target.value })
          }}
        />
      </div>
    </>
  )
}

export default PersonalDetails
