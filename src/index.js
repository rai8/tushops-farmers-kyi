import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './index.css'
import App from './App'
import MainPage from './pages/MainPage'
import TicketDetail from './pages/TicketDetail'
import AddTicket from './pages/AddTicket'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path='/' element={<App />}>
          <Route index element={<MainPage />} />
          <Route path='/ticket/:id' element={<TicketDetail />} />
          <Route path='/add-ticket' element={<AddTicket />} />
        </Route>
      </Routes>
    </Router>
  </React.StrictMode>
)
