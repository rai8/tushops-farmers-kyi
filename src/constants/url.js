// const URL_TUSHOPS = 'http://127.0.0.1:5005/api'
const BASE_URL = {
  URL_TUSHOPS: 'https://tushops-backend-farmers.herokuapp.com/api',
  URL_GATEWAY: 'http://127.0.0.1:5006/api/v1',
}

export default BASE_URL
